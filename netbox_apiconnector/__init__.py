from extras.plugins import PluginConfig


class NetboxApiConnector(PluginConfig):
    name = "netbox_apiconnector"
    verbose_name = "apiconnector"
    version = "0.1.7"
    base_url = "netbox_apiconnector"
    min_version = "3.4"
    required_settings = []
    default_settings = {
    }


config = NetboxApiConnector
