import django_filters
from django.db.models import Q
from django.utils.translation import gettext as _
from netbox.filtersets import NetBoxModelFilterSet
from netbox_apiconnector.models import ApiConnectorResult, ApiConnectorResultChange, ApiConnector


class ApiConnectorResultFilterSet(NetBoxModelFilterSet):
    connector = django_filters.ModelMultipleChoiceFilter(
        field_name='connector',
        queryset=ApiConnector.objects.all(),
        label=_('connector'),
    )

    class Meta:
        model = ApiConnectorResult
        fields = ["id"]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(connector__name__icontains=value) |
            Q(status__icontains=value)
        ).distinct()


class ApiConnectorResultChangeFilterSet(NetBoxModelFilterSet):
    run_result = django_filters.ModelMultipleChoiceFilter(
        field_name='run_result',
        queryset=ApiConnectorResult.objects.all(),
        label=_('run result'),
    )

    class Meta:
        model = ApiConnectorResultChange
        fields = ["id"]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(run_result__connector__name__icontains=value) |
            Q(status__icontains=value)
        ).distinct()
