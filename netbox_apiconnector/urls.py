from django.urls import path

from netbox.views.generic import ObjectChangeLogView, ObjectJournalView
from netbox_apiconnector import models
from netbox_apiconnector.views import *

urlpatterns = [
    path("apiconnector/", ApiConnectorListView.as_view(), name="apiconnector_list"),
    path("apiconnector/<int:pk>", ApiConnectorView.as_view(), name="apiconnector"),
    path("apiconnector/<int:pk>/delete", ApiConnectorDeleteView.as_view(), name="apiconnector_delete"),

    path("apiconnector/<int:pk>/edit", ApiConnectorEditView.as_view(), name="apiconnector_edit"),
    path('apiconnector/add', ApiConnectorEditView.as_view(), name='apiconnector_add'),
    path('apiconnector/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='apiconnector_changelog',
         kwargs={'model': models.ApiConnector}),
    path('apiconnector/<int:pk>/journal/', ObjectJournalView.as_view(), name='apiconnector_journal',
         kwargs={'model': models.ApiConnector}),
    path("apiconnector_result/", ApiConnectorResultListView.as_view(), name="apiconnectorresult_list"),
    path("apiconnector_result/<int:pk>", ApiConnectorResultView.as_view(), name="apiconnectorresult"),
    path("apiconnector_result/<int:pk>/delete", ApiConnectorResultDeleteView.as_view(), name="apiconnectorresult_delete"),
    path("apiconnector_result/delete", ApiConnectorResultBulkDeleteView.as_view(),name="apiconnectorresult_bulk_delete"),
    path("apiconnector_result/<int:pk>/edit", ApiConnectorResultEditView.as_view(), name="apiconnectorresult_edit"),
    path('apiconnector_result/add', ApiConnectorResultEditView.as_view(), name='apiconnectorresult_add'),
    path('apiconnector_result/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='apiconnectorresult_changelog',
         kwargs={'model': models.ApiConnectorResult}),
    path('apiconnector_result/<int:pk>/journal/', ObjectJournalView.as_view(), name='apiconnectorresult_journal',
         kwargs={'model': models.ApiConnectorResult}),
    path("apiconnector_result_change/", ApiConnectorResultChangeListView.as_view(), name="apiconnectorresultchange_list"),
    path('apiconnector_result/add', ApiConnectorResultChangeEditView.as_view(), name='apiconnectorresultchange_add'),
    path("apiconnector_result_change/<int:pk>", ApiConnectorResultChangeView.as_view(), name="apiconnectorresultchange"),
    path("apiconnector_result_change/<int:pk>/delete", ApiConnectorResultChangeDeleteView.as_view(),name="apiconnectorresultchange_delete"),
    path("apiconnector_result_change/delete", ApiConnectorResultChangeBulkDeleteView.as_view(),name="apiconnectorresultchange_bulk_delete"),
    path("apiconnector_result_change/<int:pk>/edit", ApiConnectorResultChangeEditView.as_view(), name="apiconnectorresultchange_edit"),
    path('apiconnector_result_change/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='apiconnectorresultchange_changelog',
         kwargs={'model': models.ApiConnectorResultChange}),
    path('apiconnector_result_change/<int:pk>/journal/', ObjectJournalView.as_view(), name='apiconnectorresultchange_journal',
         kwargs={'model': models.ApiConnectorResultChange}),
    path("apiconnector/<int:pk>/runstatic", run_static_converter, name="apiconnector_runstatic"),
    path("apiconnector/<int:pk>/rundynamic", run_dynamic_converter, name="apiconnector_rundynamic"),
    path("apiconnector/apply_conversion", apply_conversion, name="apiconnector_apply_conversion"),
    path("apiconnector/<int:pk>/apply_conversion_by_pk", apply_conversion_by_pk, name="apiconnector_apply_conversion_by_pk"),
    path("apiconnector/apply_all_conversion", apply_all_conversion, name="apiconnector_apply_all_conversion"),
    path("apiconnector/<int:pk>/apply_all_conversion_by_pk", apply_all_conversion_by_pk, name="apiconnector_apply_all_conversion_by_pk"),
]
