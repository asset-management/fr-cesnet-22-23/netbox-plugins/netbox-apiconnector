import django_tables2 as tables
from netbox.tables import NetBoxTable
from .models import *
from .template_code import *


class ApiConnectorTable(NetBoxTable):
    name = tables.Column(
        linkify=True
    )
    status = tables.TemplateColumn(template_code=TABLE_STATUS)

    class Meta(NetBoxTable.Meta):
        model = ApiConnector
        fields = ('id', 'name', 'run_in_background', 'url', 'pattern_url', 'obj', "create_new_objects", "status")
        default_columns = (
            'id', 'name', 'run_in_background', 'url', 'pattern_url', 'obj', "create_new_objects", "status")


class ApiConnectorResultTable(NetBoxTable):
    connector = tables.Column(
        linkify=True
    )
    changes = tables.TemplateColumn(template_code=RESULT_CHANGES_COUNT)
    apply = tables.TemplateColumn(template_code=RESULT_APPLY_BUTTON)
    status = tables.TemplateColumn(template_code=TABLE_STATUS)

    class Meta(NetBoxTable.Meta):
        model = ApiConnectorResult
        fields = ('id', 'connector', 'changes', 'date', 'last_applied', 'status', 'apply')
        default_columns = ('id', 'connector', 'changes', 'date', 'last_applied', 'status', 'apply')


class ApiConnectorResultChangeTable(NetBoxTable):
    run_result = tables.Column(
        linkify=True
    )
    current_value = tables.JSONColumn(json_dumps_kwargs={"ensure_ascii": False, "indent": 2})
    new_value = tables.JSONColumn(json_dumps_kwargs={"ensure_ascii": False, "indent": 2})
    obj = tables.TemplateColumn(
        template_code=CHANGE_OBJ,
        verbose_name="Object to be changed", )
    apply = tables.TemplateColumn(template_code=RESULT_CHANGE_APPLY_BUTTON)
    status = tables.TemplateColumn(template_code=TABLE_STATUS)

    class Meta(NetBoxTable.Meta):
        model = ApiConnectorResultChange
        fields = ('id', 'run_result', 'obj', 'current_value', 'new_value', 'last_applied', 'status', 'apply')
        default_columns = ('id', 'run_result', 'obj', 'current_value', 'new_value', 'last_applied', 'status', 'apply')
