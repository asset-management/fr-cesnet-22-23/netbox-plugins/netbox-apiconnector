from netbox_apiconnector.models import *
from django.contrib import admin


@admin.register(ApiConnector)
class ApiConnectorAdmin(admin.ModelAdmin):
    list_display = ("name", "run_in_background", "url", "pattern_url",
                    "obj", "mapping",
                    "additional_headers", "additional_body")


@admin.register(ApiConnectorResult)
class ApiConnectorResultAdmin(admin.ModelAdmin):
    list_display = ("connector", "date", "last_applied")

@admin.register(ApiConnectorResultChange)
class ApiConnectorResultAdmin(admin.ModelAdmin):
    pass
