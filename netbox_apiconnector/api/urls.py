# api/urls.py
from netbox.api.routers import NetBoxRouter
from .views import *

router = NetBoxRouter()
router.register('apiconnector', ApiConnectorViewSet)
router.register('apiconnectorresult', ApiConnectorResultViewSet)
router.register('apiconnectorresultchange', ApiConnectorResultChangeViewSet)
urlpatterns = router.urls
