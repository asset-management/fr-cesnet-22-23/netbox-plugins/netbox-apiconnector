from extras.forms import LocalConfigContextFilterForm
from netbox.forms import NetBoxModelFilterSetForm
from netbox_apiconnector.models import ApiConnectorResult, ApiConnectorResultChange, ApiConnector
from tenancy.forms import TenancyFilterForm
from django.utils.translation import gettext as _
from utilities.forms.fields import TagFilterField, DynamicModelMultipleChoiceField


class ApiConnectorResultForm(NetBoxModelFilterSetForm):
    model = ApiConnectorResult
    fieldsets = (
        ('connector', ('connector',),),
    )
    connector = DynamicModelMultipleChoiceField(
        queryset=ApiConnector.objects.all(),
        required=False,
        query_params={
            'connector': '$id'
        },
        label=_('Connector')
    )

    tag = TagFilterField(model)


class ApiConnectorResultChangeForm(NetBoxModelFilterSetForm):
    model = ApiConnectorResultChange
    fieldsets = (
        ('Run result', ('run_result',),),
    )
    run_result = DynamicModelMultipleChoiceField(
        queryset=ApiConnectorResult.objects.all(),
        required=False,
        query_params={
            'run_result': '$id'
        },
        label=_('Run result')
    )

    tag = TagFilterField(model)
