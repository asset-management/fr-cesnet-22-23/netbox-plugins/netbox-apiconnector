from dcim.models import Site
from netbox.forms import NetBoxModelForm
from utilities.forms.fields import CommentField, DynamicModelChoiceField
from netbox_apiconnector.models import *


class ApiConnectorForm(NetBoxModelForm):
    # site = DynamicModelChoiceField(
    #     queryset=Site.objects.all()
    # )
    # comments = CommentField()
    fieldsets = (
        ('Friendly name', ('name',),),
        ('Connection', ('method', 'content_type', 'url', 'pattern_url'),),
        ('Behavior', ('obj', 'run_in_background', 'create_new_objects',),),
        ('Jinja2 mapping', ('mapping',),),
        ('Additional header and body', ('additional_headers', 'additional_body',),)
    )

    class Meta:
        model = ApiConnector
        fields = ('name', 'method', 'content_type', 'run_in_background', 'url', 'pattern_url', 'obj',
                  'create_new_objects', 'mapping', 'additional_headers', 'additional_body')


class ApiConnectorResultForm(NetBoxModelForm):
    # site = DynamicModelChoiceField(
    #     queryset=Site.objects.all()
    # )
    # comments = CommentField()
    # fieldsets = (
    #     ('Model Stuff', ('name', 'url', 'obj', 'jinja')),
    # )

    class Meta:
        model = ApiConnectorResult
        fields = ()


class ApiConnectorResultChangeForm(NetBoxModelForm):
    # site = DynamicModelChoiceField(
    #     queryset=Site.objects.all()
    # )
    # comments = CommentField()
    # fieldsets = (
    #     ('Model Stuff', ('name', 'url', 'obj', 'jinja')),
    # )

    class Meta:
        model = ApiConnectorResultChange
        fields = ('new_value',)
