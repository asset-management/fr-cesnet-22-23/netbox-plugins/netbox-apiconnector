from django import template
from django.template import Template, Context
from netbox_apiconnector.helpers.database_helpers import try_get
from netbox_apiconnector.models import ApiConnectorResultChange
from django.apps import apps
from netbox_apiconnector.template_code import *

register = template.Library()


@register.simple_tag
def get_url(change: ApiConnectorResultChange):
    m = apps.get_model(change.run_result.connector.obj.app_label, change.run_result.connector.obj.model)
    obj = try_get(m, change.obj)
    if obj is not None:
        return obj.get_absolute_url()
    return None


@register.simple_tag
def get_name(change: ApiConnectorResultChange):
    m = apps.get_model(change.run_result.connector.obj.app_label, change.run_result.connector.obj.model)
    obj = try_get(m, change.obj)
    if obj is not None:
        return obj
    return None


@register.simple_tag
def wrap_status(record):
    t = Template(TABLE_STATUS)
    c = Context({"record": record})
    return t.render(c)


@register.simple_tag
def wrap_obj(record):
    t = Template(CHANGE_OBJ)
    c = Context({"record": record})
    return t.render(c)
