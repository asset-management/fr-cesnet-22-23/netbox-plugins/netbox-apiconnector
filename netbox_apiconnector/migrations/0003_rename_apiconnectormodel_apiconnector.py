# Generated by Django 4.1.7 on 2023-03-27 10:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('extras', '0084_staging'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('netbox_apiconnector', '0002_apiconnectormodel_custom_field_data_and_more'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ApiConnectorModel',
            new_name='ApiConnector',
        ),
    ]
