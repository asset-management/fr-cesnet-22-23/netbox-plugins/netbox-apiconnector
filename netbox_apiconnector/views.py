from django.contrib import messages
from django.shortcuts import redirect

from netbox.views import generic
from netbox_apiconnector import tables, filtersets
from netbox_apiconnector.forms import filtersets as form_filtersets
from netbox_apiconnector.forms.forms import ApiConnectorForm, ApiConnectorResultForm, ApiConnectorResultChangeForm
from netbox_apiconnector.api_connector import *
from netbox_apiconnector.models import ApiConnector, ApiConnectorResult, ApiConnectorResultChange
from netbox_apiconnector.tables import ApiConnectorTable, ApiConnectorResultTable
from .status_codes import *


# API CONNECTOR
class ApiConnectorView(generic.ObjectView):
    queryset = ApiConnector.objects.all()
    template_name = 'netbox_apiconnector/apiconnector.html'
    table = ApiConnectorTable

    # def get(self, request, *args, **kwargs):
    #     print(request.GET)
    #     return super().get(request, *args, **kwargs)


class ApiConnectorListView(generic.ObjectListView):
    queryset = ApiConnector.objects.all()
    # filterset = filtersets.VirtualMachineFilterSet
    # filterset_form = forms.VirtualMachineFilterForm
    table = tables.ApiConnectorTable
    template_name = 'netbox_apiconnector/apiconnector_list.html'


class ApiConnectorEditView(generic.ObjectEditView):
    queryset = ApiConnector.objects.all()
    form = ApiConnectorForm
    template_name = 'generic/object_edit.html'
    # additional_permissions = ['dcim.view_device', 'extras.view_tags']


class ApiConnectorDeleteView(generic.ObjectDeleteView):
    queryset = ApiConnector.objects.all()


# API CONNECTOR RESULT
class ApiConnectorResultView(generic.ObjectView):
    queryset = ApiConnectorResult.objects.all()
    template_name = 'netbox_apiconnector/apiconnectorresult.html'
    table = ApiConnectorResultTable


class ApiConnectorResultListView(generic.ObjectListView):
    queryset = ApiConnectorResult.objects.all()
    filterset = filtersets.ApiConnectorResultFilterSet
    filterset_form = form_filtersets.ApiConnectorResultForm
    table = tables.ApiConnectorResultTable
    template_name = 'netbox_apiconnector/apiconnectorresult_list.html'


class ApiConnectorResultEditView(generic.ObjectEditView):
    queryset = ApiConnectorResult.objects.all()
    form = ApiConnectorResultForm
    template_name = 'generic/object_edit.html'
    # additional_permissions = ['dcim.view_device', 'extras.view_tags']


class ApiConnectorResultDeleteView(generic.ObjectDeleteView):
    queryset = ApiConnectorResult.objects.all()


class ApiConnectorResultBulkDeleteView(generic.BulkDeleteView):
    queryset = ApiConnectorResult.objects.all()
    filterset = filtersets.ApiConnectorResultFilterSet
    table = tables.ApiConnectorResultTable


# API CONNECTOR RESULT CHANGES
class ApiConnectorResultChangeView(generic.ObjectView):
    queryset = ApiConnectorResultChange.objects.all()
    template_name = 'netbox_apiconnector/apiconnectorresultchange.html'
    table = tables.ApiConnectorResultChangeTable


class ApiConnectorResultChangeListView(generic.ObjectListView):
    queryset = ApiConnectorResultChange.objects.all()
    filterset = filtersets.ApiConnectorResultChangeFilterSet
    filterset_form = form_filtersets.ApiConnectorResultChangeForm
    table = tables.ApiConnectorResultChangeTable
    template_name = 'netbox_apiconnector/apiconnectorresultchange_list.html'


class ApiConnectorResultChangeEditView(generic.ObjectEditView):
    queryset = ApiConnectorResultChange.objects.all()
    form = ApiConnectorResultChangeForm
    template_name = 'generic/object_edit.html'
    # additional_permissions = ['dcim.view_device', 'extras.view_tags']


class ApiConnectorResultChangeDeleteView(generic.ObjectDeleteView):
    queryset = ApiConnectorResultChange.objects.all()


class ApiConnectorResultChangeBulkDeleteView(generic.BulkDeleteView):
    queryset = ApiConnectorResultChange.objects.all()
    filterset = filtersets.ApiConnectorResultChangeFilterSet
    table = tables.ApiConnectorResultChangeTable


def run_static_converter(request, pk):
    """
    Runs the standard API connector (uses the 'URL' field from the form). It either starts
    a new thread or runs to connector in the foreground.
    """
    connector = ApiConnector.objects.get(pk=pk)
    if connector.run_in_background:
        start_thread(static_converter_worker, connector)
        messages.info(request, "Running in background")
        return redirect(request.META['HTTP_REFERER'])
    try:
        static_converter(connector)
        messages.success(request, "Successfully loaded")
    except Exception as e:
        set_status(connector, FAIL, e)
        messages.error(request, f"{e}")
    return redirect(request.META['HTTP_REFERER'])


def run_dynamic_converter(request, pk):
    """
    Runs the pattern API connector (uses the 'Pattern URL' field from the form). It either starts
    a new thread or runs to connector in the foreground.
    """
    connector = ApiConnector.objects.get(pk=pk)
    if connector.run_in_background:
        start_thread(dynamic_converter_worker, connector)
        messages.info(request, "Running in background")
        return redirect(request.META['HTTP_REFERER'])
    try:
        dynamic_converter(connector)
        messages.success(request, "Successfully loaded")
    except Exception as e:
        set_status(connector, FAIL, e)
        messages.error(request, f"{e}")
    return redirect(request.META['HTTP_REFERER'])


def apply_conversion(request):
    """
    Applies a list of changes in a new thread.
    Used by the 'apply selected' button in the 'Changes' pane.
    """
    if request.method != "POST":
        messages.error(request, "Error, not a POST request")
    else:
        start_thread(apply_changes_by_pk, request.POST.getlist('pk'))
        messages.info(request, "Applying in background")
    return redirect(request.META['HTTP_REFERER'])


def apply_conversion_by_pk(request, pk):
    """
    Applies a single change in the foreground.
    Used by the apply button in the Changes pane.
    """
    if apply_changes_by_pk([pk]) == 0:
        messages.success(request, "Successfully applied")
    else:
        messages.error(request, "Failed, see message for details")
    return redirect(request.META['HTTP_REFERER'])


def apply_all_conversion(request):
    """
    Applies a list of results (containing multiple changes) in a new thread.
    Used by the 'apply selected' button in the 'Results' pane.
    """
    if request.method != "POST":
        messages.error(request, "Error, not a POST request")
    else:
        start_thread(apply_results_by_pk, request.POST.getlist('pk'))
        messages.info(request, "Applying in background")
    return redirect(request.META['HTTP_REFERER'])


def apply_all_conversion_by_pk(request, pk):
    """
    Applies a single 'result' in a new thread.
    Used by the apply button in the 'Results' pane.
    """
    start_thread(apply_results_by_pk, [pk])
    messages.info(request, "Applying in background")
    return redirect(request.META['HTTP_REFERER'])
